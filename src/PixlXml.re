let handleOptBool = optBool => optBool
  |> Js.Option.map([@bs] bool => bool ? Js.true_ : Js.false_)
  |> Js.Nullable.fromOption;

type parseOptions = {. 
  "forceArrays": Js.Nullable.t(Js.boolean), 
  "preserveAttributes": Js.Nullable.t(Js.boolean),
  "lowerCase": Js.Nullable.t(Js.boolean),
  "preserveDocumentNode": Js.Nullable.t(Js.boolean),
  "preserveWhitespace": Js.Nullable.t(Js.boolean)
};

[@bs.module "pixl-xml"] external internal_parse : (string, parseOptions) => _ = "parse";
let parse = ( ~forceArrays=?, ~preserveAttributes=?, ~lowerCase=?, ~preserveDocumentNode=?, ~preserveWhitespace=?, xml:string) => {
  internal_parse(xml, {
    "forceArrays": forceArrays |> handleOptBool,
    "preserveAttributes": preserveAttributes |> handleOptBool,
    "lowerCase": lowerCase |> handleOptBool,
    "preserveDocumentNode": preserveDocumentNode |> handleOptBool,
    "preserveWhitespace": preserveWhitespace |> handleOptBool
  });
};

type xml;
[@bs.module "pixl-xml"] external internal_stringify : ( xml, Js.Nullable.t(Js.String.t) ) => string = "stringify";
let stringify = (~root=?, doc:_) => {
  internal_stringify(doc, root |> Js.Nullable.fromOption);
};